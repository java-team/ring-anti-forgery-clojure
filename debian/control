Source: ring-anti-forgery-clojure
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               javahelper,
               maven-repo-helper,
               clojure,
               libtext-markdown-perl | markdown,
               libcrypto-random-clojure,
               libcrypto-equality-clojure,
               libhiccup-clojure,
               libring-mock-clojure,
               default-jdk-headless
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/clojure-team/ring-anti-forgery-clojure
Vcs-Git: https://salsa.debian.org/clojure-team/ring-anti-forgery-clojure.git
Homepage: https://github.com/ring-clojure/ring-anti-forgery

Package: libring-anti-forgery-clojure
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends}
Recommends: ${java:Recommends}
Description: Ring middleware to prevent CSRF attacks
 ring-anti-forgery is a Ring middleware that prevents CSRF attacks via
 a randomly-generated anti-forgery token. By default, any request that isn't a
 HEAD or GET request will require an anti-forgery token, or an "access denied"
 response will be returned. The token is bound to the session, and accessible
 via the "anti-forgery-token" variable.
